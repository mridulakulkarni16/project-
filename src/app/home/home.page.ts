import { LoginPageModule } from './../login/login.module';
import { LoginPage } from './../login/login.page';
import { Component } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { RestService } from './../rest.service';
import { Platform } from '@ionic/angular';
import { NavController  } from '@ionic/angular';

import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private db: SQLite;
  user: any;
  USERNAME: any;
  ID: any;
  TITLE: any;
  BODY: any;
  expenses: any = [];
  Expense1: any;
  Expense2: any;
  Expense3: any;
  Expense4: any;
  id:  any;
  username: any;
  title: any;
  body: any;
  hideme: any = [];
   hide: any = [];
  // tslint:disable-next-line:no-inferrable-types
  show: boolean = true;
  // tslint:disable-next-line:no-inferrable-types
  // hide: boolean = true;
  // tslint:disable-next-line:max-line-length
  constructor(public navCtrl: NavController, private platform: Platform , public restservice: RestService, private sqlite: SQLite , public router: Router) {
this.getUsers();
 }
 ionViewDidLoad() {
  console.log('-------ppppppppppppppp------' + this.user.shareinfo.username);
}

getData() {
  console.log('Json return response' +  JSON.stringify(this.user));
this.platform.ready().then(() => {
    this.db = new SQLite();
    this.sqlite.create({name: 'mydatabase.db', location: 'default'}).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS jsontablee(id INTEGER PRIMARY KEY  , username TEXT , title TEXT , body TEXT)', [])
      .then(() => console.log('TABLE CREATED'))
      .catch(e => console.log(e));
      db.executeSql('DELETE FROM jsontablee');

      console.log('-------jjkkjkj------' );

      // tslint:disable-next-line:max-line-length

      // tslint:disable-next-line:no-var-keyword
      for ( var i = 0; i < this.user.shareinfo.length; i++) {

        const USERNAME = this.user.shareinfo[i].username;
        const ID = this.user.shareinfo[i].id;
        const TITLE = this.user.shareinfo[i].title;
        const BODY = this.user.shareinfo[i].body;
        //       //   // tslint:disable-next-line:prefer-const
              // tslint:disable-next-line:prefer-const
              let query = 'INSERT  OR IGNORE INTO jsontablee (id, username , title , body) VALUES (?,?,?,?)';

        //       //   // tslint:disable-next-line:max-line-length
             db.executeSql(query, [ID, USERNAME, TITLE, BODY])
       .then(res => {

      }, (error) => {
        console.log('ERROR: ' + JSON.stringify(error));


    });
    console.log((query.match(new RegExp('\\?', 'g')) || []).length);
  }console.log('Inserted res value:');



    // tslint:disable-next-line:max-line-length

  });
});

}
getData1() {
  this.sqlite.create({
    name: 'mydatabase.db',
  location: 'default'
     }).then((db: SQLiteObject) => {
  db.executeSql('SELECT * FROM jsontablee', [])
  .then(res => {
     this.expenses = [];
     // tslint:disable-next-line:no-var-keyword
     // tslint:disable-next-line:no-shadowed-variable
     for (let i = 0; i < res.rows.length; i++) {
      // tslint:disable-next-line:max-line-length
      this.expenses.push({id: res.rows.item(i).id , username: res.rows.item(i).username , title: res.rows.item(i).title , body: res.rows.item(i).body});

     }
     console.log('expenses:------' +  JSON.stringify (this.expenses));
    });
  })
  .catch(e => console.log('----------select error' + e));
}

getUsers() {
  this.restservice.getUsers1()
  .then(data => {
this.user = data;

    // tslint:disable-next-line:prefer-const
    // let count = Object.keys(this.user.shareinfo.length);
    // console.log(count);
    console.log('---------------****************----------------' + this.user.shareinfo.length);

   // this.getData();
   this.getData1();

 });
}

ngIfCtrl(index) {
  const i = index;
  // alert('index is' + i);
  this.hide[i] = !this.hide[i];
}

/* updatedata() {

console.log('success1:::' + this.expenses[0].title);
 const Expense1 = this.expenses.id;
 const Expense2 = this.expenses.username;
 const Expense3 = this.expenses.title;
 const Expense4 = this.expenses.body;
  // const item = {id: '', username: '', title: '', body: '' };

   this.sqlite.create({
    name: 'mydatabase.db',
    location: 'default'
  }).then((db: SQLiteObject) => {

 // tslint:disable-next-line:max-line-length
 db.executeSql('UPDATE  jsontablee SET username=?,title=?,body=?  WHERE id=?', [ this.username, this.title,  this.body , this.id])
 .then(res => {
  console.log('updated' + JSON.stringify(res));
 });
 })
 .catch(e => {
 console.log(e);
 });

} */
openPage(index) {
// alert('index is' + index);
const  getindex = index;
console.log('success1:::' + this.expenses[getindex].title);
 const Expense1 = this.expenses[getindex].id;
 const Expense2 = this.expenses[getindex].username;
 const Expense3  = this.expenses[getindex].title;
 const Expense4 = this.expenses[getindex].body;
  // const item = {id: '', username: '', title: '', body: '' };

   this.sqlite.create({
    name: 'mydatabase.db',
    location: 'default'
  }).then((db: SQLiteObject) => {

 // tslint:disable-next-line:max-line-length
 db.executeSql('UPDATE  jsontablee SET username=?,title=?,body=?  WHERE id=?', [ Expense2, Expense3,  Expense4 , Expense1])
 .then(res => {
  console.log('updated' + JSON.stringify(res));
 });
 })
 .catch(e => {
 console.log(e);
 });

}

Login() {
  this.navCtrl.navigateRoot('/login');
 this.router.navigateByUrl('/login');
}
// onClick(item) {
//   Object.keys(this.hideme).forEach(h => {
//     this.hideme[h] = false;
//   });
//   this.hideme[item.id] = true;

}




