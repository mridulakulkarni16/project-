import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public nvctrl: NavController , public router: Router) { }

  ngOnInit() {

  }
  Loginn() {

    this.router.navigateByUrl('/register');

  }
}
