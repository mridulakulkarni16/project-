import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(public nvctrl: NavController , public router: Router) { }

  ngOnInit() {
  }
Home() {

    this.router.navigateByUrl('/home');

}

Login1() {
  this.router.navigateByUrl('/login');
}
}
